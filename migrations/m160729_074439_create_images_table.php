<?php

use yii\db\Migration;

/**
 * Handles the creation for table `images`.
 */
class m160729_074439_create_images_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('images', [
            'id' => $this->primaryKey(),
            'image' => $this->string(90)->notNull(),
            'title' => $this->string(90)->notNull(),
            'slug' => $this->string(90)->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('images');
    }
}
