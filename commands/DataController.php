<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Image;
use app\models\User;
//use app\models\Image;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DataController extends Controller
{
    private $date_time;

    public function actionCreate()
    {
        $this->date_time = date("y-m'd H:i:s", time());

        foreach ($this->users as $user) {
            $this->createUsers($user);
        }

        foreach ($this->images as $image) {
            $this->createImages($image);
        }


        echo 'Done!!!';
    }

    private function createUsers($user)
    {
        $user_modal = new User();
        $user_modal->email = $user['email'];
        $user_modal->setPassword($user['password']);
        $user_modal->username = $user['username'];
        $user_modal->created_at = $this->date_time;
        $user_modal->updated_at = $this->date_time;

        $user_modal->save();
    }

    private function createImages($image)
    {
        $user_modal = new Image();
        $user_modal->image = $image['image'];
        $user_modal->title = $image['title'];
        $user_modal->setSlug($image['title']);
        $user_modal->created_at = $this->date_time;
        $user_modal->updated_at = $this->date_time;

        $user_modal->save();
    }


    private $users = [
        ['username' => 'Dima', 'email' => 'Dima@mail.ru', 'password' => 123456,],
        ['username' => 'Test', 'email' => 'Test@mail.ru', 'password' => 123456,],
        ['username' => 'Misha', 'email' => 'Misha@mail.ru', 'password' => 123456,],
    ];
    private $images = [
        ['image' => 'image-1.jpg', 'title' => 'title 1',],
        ['image' => 'image-2.jpg', 'title' => 'title 2',],
        ['image' => 'image-3.jpg', 'title' => 'title 3',],
        ['image' => 'image-4.jpg', 'title' => 'title 4',],
        ['image' => 'image-5.jpg', 'title' => 'title 5',],
        ['image' => 'image-6.jpg', 'title' => 'title 6',],
    ];
}
