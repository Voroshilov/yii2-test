<?php
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>

<?php
    $form = ActiveForm::begin(['class' => 'form-horizontal']);
?>
<div class="site-index">

    <div class="jumbotron">
        <h3>Register!</h3>
    </div>

    <div class="col-md-8 col-md-offset-2">

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]);?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true]);?>

        <?= $form->field($model, 'password')->passwordInput();?>

        <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>

        <div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</div>

<?php
    ActiveForm::end();
?>

