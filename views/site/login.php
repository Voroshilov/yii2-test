<?php

use yii\bootstrap\ActiveForm;

//$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(); ?>

<div class="site-login">

    <div class="jumbotron">
        <h3>Login</h3>
    </div>

    <div class="col-md-8 col-md-offset-2">

        <?= $form->field($login_model, 'email')->textInput(['autofocus' => true]);?>

        <?= $form->field($login_model, 'password')->passwordInput() ?>

        <div>
            <button class="btn btn-primary">Login</button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
