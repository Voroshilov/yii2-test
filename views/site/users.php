<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div>
                        <!-- Инициализация виджета "Bootstrap datetimepicker" -->
                        <div class="form-group">
                            <!-- Элемент HTML с id равным datetimepicker1 -->
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <tr>
                            <td></td>
                            <td>Name</td>
                            <td>E-mail</td>
                            <td>Created date</td>
                        </tr>

                        <?php
                        $count = 0;
                        foreach ($users as $user) {
                            $count++;
                            ?>

                            <tr>
                                <td><?php echo $count; ?></td>
                                <td><?php echo $user['username']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><?php echo date('d.m.Y H:i', strtotime($user['created_at'])); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>