<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;

class Image extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'images';
    }
    //create slug from title
    public function setSlug($slug)
    {
        $pattern = '/\s/';
        $replacement = '-';
        $this->slug = strtolower(preg_replace($pattern, $replacement, $slug));
    }

    //Method form interface
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    //Get all images
    public function getAll()
    {
        return Yii::$app->db->createCommand('SELECT * FROM {{images}} ORDER BY created_at DESC ')->queryAll();
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    public function getAuthKey()
    {

    }

    public function validateAuthKey($authKey)
    {

    }
}
