<?php

namespace app\models;

use yii\base\Model;

class Register extends Model
{
    public $username;
    public $email;
    public $password;
    public $captcha;
    public $created_at;
    public $updated_at;

    public function rules()
    {
        return [
            [['username', 'email', 'password', 'captcha'], 'required'],
            ['username', 'string', 'min' => 2, 'max' => 60],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User'],
            ['captcha', 'captcha'],
            ['password', 'string', 'min' => 6, 'max' => 20],
        ];
    }

    public function register()
    {
        $date_time = date("y-m'd H:i:s", time());

        $user = new User();
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->username = $this->username;
        $user->created_at = $date_time;
        $user->updated_at = $date_time;

        return $user->save();
    }
}