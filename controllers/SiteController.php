<?php

namespace app\controllers;

use app\models\Image;
use app\models\Login;
use app\models\Register;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\gii\Module;
use yii\web\Controller;
use yii\filters\VerbFilter;
//use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    return $this->goHome();
                },
                'only' => ['login', 'logout', 'register', 'users'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'register'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'users'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Index page
     * @return array images
     */
    public function actionIndex()
    {
        $image_model = new Image();
        $images = $image_model->getAll();

        return $this->render('index', ['images' => $images]);
    }

    /**
     * Show register page and performs registration
     * @return object| /and redirect to index page
     */
    public function actionRegister()
    {
        $model = new Register();

        if(isset($_POST['Register'])){
            $model->attributes = Yii::$app->request->post('Register');
            if($model->validate()){
                $model->register();
                return $this->goHome();
            }
        }

        return $this->render('register', ['model' => $model]);
    }
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    /**
     * Login Method
     * @return object| login user data and redirect to index page
     */
    public function actionLogin()
    {
        $login_model = new Login();

        if(Yii::$app->request->post('Login')){
            $login_model->attributes = Yii::$app->request->post('Login');

            if($login_model->validate()){
                Yii::$app->user->login($login_model->getUser());
                return $this->goHome();
            }
        }

        return $this->render('login', ['login_model' => $login_model]);
    }

    /**
     * Logout method
     * @redirect to index page
     */
    public function actionLogout()
    {
        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
            return $this->redirect(['login']);
        }
    }


    /**
     * Show all users
     * @return array all users
     */
    public function actionUsers()
    {
//        if(Yii::$app->user->isGuest){
//            return $this->goHome();
//        }

        $user_model = new User();
        $users = $user_model->getAll();

        return $this->render('users', ['users' => $users]);
    }

    /**
     * Home page
     * @return array from user data
     */
    public function actionHome()
    {
        $user_model = new User();
        $user = $user_model->findIdentity(Yii::$app->user->id);

        return $this->render('home', ['user' => $user]);
    }
}
